;   Copyright (C) 2020 Anatoly Mihailov (TolyProg)
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <https://www.gnu.org/licenses/>.

; This program is displaying .TXT ASCII text

use32

?cat:
mov ah,al
@cat:
cmp ecx,0
jz ?ret
dec ecx
lodsb
cmp al,0ah
jz @newstring
push ecx
mov ecx,1
mov [catbuffer],al
push esi
mov esi,catbuffer
mov al,ah
call ?print
pop esi
pop ecx
inc dl
mov [catbuffer],0
jmp @cat
@newstring:
inc dh
mov dl,0
jmp @cat

catbuffer db 0