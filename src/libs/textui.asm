;   Copyright (C) 2020 Anatoly Mihailov (TolyProg)
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program. If not, see <https://www.gnu.org/licenses/>.

;______________________________________
;         Libs\textui                  |
;______________________________________|
; "@" is a label                       |
; "?" is a function                    |
;______________________________________|

use32

?printInCenter:
push bx
push cx
push ax

mov dh,25
div dh

pop ax
pop cx
pop bx

;mov dh,12
;mov dl,40-7

call ?print
ret

jmp ?panic