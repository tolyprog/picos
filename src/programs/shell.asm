;   Copyright (C) 2020 Anatoly Mihailov (TolyProg)
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <https://www.gnu.org/licenses/>.

;______________________________________
;       PicOS\Shell                    |
;______________________________________|
; "@" is a label                       |
; "?" is a function                    |
;______________________________________|

use32

?shell:

mov al,3
mov cx,59 ; Size of text in bytes
mov dh,3 ; X
mov dl,0 ; Y
mov esi,hitext
call ?cat

mov dh,6
mov al,2
mov dl,0
mov esi,prompt
call ?printsym

mov al,2
mov dl,2
mov dh,6
mov esi,buff
call ?printsym

mov dh,6
mov dl,2
call ?setcursorxy

@shellloop:

call ?getkey

cmp al,01h
jz ?bye

call ?scan2ascii

mov [buff],al

mov al,2
mov dl,2
mov dh,6
mov esi,buff
call ?printsym

jmp @shellloop

jmp ?panic

?bye:

mov cx,7
mov al,4
mov esi,byemsg
call ?printInCenter

jmp $

jmp ?halt

jmp ?panic

; Include basic programs

include '../programs/cat.asm'

jmp ?panic

buff db 0
prompt db ">"
byemsg db "Exited."
hitext: file '../text.txt'