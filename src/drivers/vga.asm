;   Copyright (C) 2020 Anatoly Mihailov (TolyProg)
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <https://www.gnu.org/licenses/>.

;______________________________________
;       PicOS\VGA                      |
;______________________________________|
; "@" is a label                       |
; "?" is a function                    |
;______________________________________|

use32

								?print:
pusha
call ?printtrans

@print: ; Print loop

lodsb ;     modifing  register
stosb ; This        AL
mov al,ah
stosb ; Print symbol
loop @print

popa
ret

jmp ?panic  ; #########################################################

								?printsym:
pusha

call ?printtrans
lodsb ;     modifing  register
stosb ; This        AL
mov al,ah
stosb ; Print symbol

popa
ret

jmp ?panic ; #########################################################

								?setcursorxy:
pusha

; al = x
; ah = y

movzx dx,al
movzx ax,ah
add dx,ax

; dx = offset
mov bx,dx
mov al,0x0E
mov ah,bh
mov dx,0x3D4
out dx,ax
inc ax
mov ah,bl
out dx,ax

popa
ret

jmp ?panic  ; #########################################################

								?printtrans:
push ax
push dx

mov eax,0
pop dx
mov edi,0b8000h
mov al,2
mul dl
add edi,eax

mov eax,0
mov al,0a0h
mul dh
add edi,eax

pop ax
mov ah,al

ret

jmp ?panic  ; #########################################################

								?colsAndStrg:

pusha

mov bl,0
cmp [videoMode],bl
jz @v0

mov bl,1
cmp [videoMode],bl
jz @v1

jmp ?panic

@v0:
popa
mov al,40
mov ah,25
ret

jmp ?panic

@v1:
popa
mov al,80
mov ah,25
ret

jmp ?panic

include '../../config/vga.cfg'