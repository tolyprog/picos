;   Copyright (C) 2020 Anatoly Mihailov (TolyProg)
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <https://www.gnu.org/licenses/>.

;______________________________________
;       PicOS\Bootloader               |
;______________________________________|
; "@" is a label                       |
; "?" is a function                    |
;______________________________________|

include '../../config/bootloader.cfg'

org 7c00h
use16

mov ah,0
mov al,2h
int 10h

mov ah,42h
mov si,@struct
mov dl,80h
int 13h
jc ?error ; Print error message

lgdt [@gdtr] ; Load GDT

in al,0x92 ; Turn on A20. Allow addressing over 1M, up to 4 GB
or al,2
out 0x92,al

cli ; Disable interrupts

in  al, 0x70
or  al, 0x80
out 0x70, al

mov eax,cr0 ; Turn on PE bit in CR0 (I.e. turn on protected mode)
or al,1
mov cr0,eax

jmp 0x8:@promodeinit ; Initialize cs by this jump. 32 bit instructions will be after that

use32 ; Generate 32 bit code
@promodeinit:

mov ax,10h
mov ds, ax
mov ss, ax
mov es, ax
mov fs, ax
mov gs, ax

mov al,2
mov ecx,(endofboot-okmsgboot)
mov dh,0
mov dl,0
mov esi,okmsgboot ; Take address to message
call ?print ; Print 'Protected mode activated!'

jmp @kernelstart ; Jump to kernel

?error: ; Function that displays an error message, then stop CPU
 mov ax,1301h
 mov bp,errmsg
 mov dl,42 - (errmsgend - errmsg) ; Set      cordinate  the      of
 mov dh,10                        ;    string         to   centre  screen
 mov cx,errmsgend - errmsg
 mov bx,0004h
 int 10h
 cli
 hlt

errmsg db 'Error!' ; Error message
errmsgend:

@struct: ; BIOS structure for loading the kernel
db 10h
db 00h
dw kernelSize         ;  | See ../../config/bootloader.cfg
dd kernelStartAddress ; /
dq 0000000000000001h
dq 0000000000000000h

@gdt:      ; Address for the GDT
@gdt_null: ; Null Segment
dq 0

@gdt_kernel: ; Kernel segment, read/execute, nonconforming
 dw 0FFFFh
 db 0,0,0
 db 10011010b
 db 11001111b
 db 0

@gdt_data: ; Data segment, read/write, expand down
 dw 0FFFFh
 db 0,0,0
 db 10010010b
 db 11001111b
 db 0

@gdtr:
    dw @gdtr-@gdt_null-1
    dd @gdt

okmsgboot:
pmodemsg db 'Protected mode activated!'
endofboot:
times 510 - ($ - $$) db 0 ;Fill with zeros to make 512 bytes
bootsign db 0x55,0xAA ;Bootloader signature for BIOS