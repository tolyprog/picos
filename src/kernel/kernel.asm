;   Copyright (C) 2020 Anatoly Mihailov (TolyProg)
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <https://www.gnu.org/licenses/>.

;______________________________________
;       PicOS\Kernel                   |
;______________________________________|
; "?" is a function                    |
; "@" is a label                       |
;______________________________________|

include 'bootloader.asm'

@kernelstart:
use32 ; Generate 32 bit code
org 7e00h

;       ;multiboot spec
;       align 4
;       dd 0x1BADB002              	;magic
;       dd 0x00                    	;flags
;       dd - (0x1BADB002 + 0x00)   	;checksum. m+f+c should be zero

;sti ; Enable interrupts

mov al,9
mov ecx,@kloadmsgend - kloadmsg
mov dh,1
mov dl,0
mov esi,kloadmsg
call ?print ; Print 'Kernel working!'

; Initial user environment

include '../init.asm'

jmp ?panic

; Drivers

include '../drivers/keyboard.asm'
include '../drivers/vga.asm'

; Libaries

include '../libs/scan2ascii.asm'
include '../libs/textui.asm'

; Kernel functions

?halt:
cli
hlt
jmp $

jmp ?panic

?reboot:
cli
mov dx,0cf9h
mov ax,0ffh
out dx,al

mov ax,'1'
push ax

jmp ?panic

?ret:
ret

?panic:
sti
mov edi,0B8000h
mov esi,panicmsg
mov al,12
mov ecx,70
mov ah,al
@printpanic:
lodsb
stosb
mov al,12
stosb
dec ecx
loop @printpanic
@retpanic:
cli
hlt
jmp $

jmp ?panic

kloadmsg db 'Kernel working!'
@kloadmsgend:
panicmsg db 'Fatal error. System halted.'

; Configs:

include '../../config/system.cfg'

times 2048 - ($ - @kernelstart) db 0