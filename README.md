# PicOS #

# This is old repository. New is on GitHub! #

This OS written on flat assembler.
This OS tries to be KISS(Keep It Simple Stupid), but at the same time has everything that users and developers need.
I am trying to create an intuitive and minimalistic, but with all that you need software and APIs.

### Kernel type: MONOLYTHIC. License: GNU GPL 3. System type: Unix-like ###
Now, PicOS is just starting to take shape.

## You may read [PicOS wiki](https://bitbucket.org/tolyprog/picos/wiki/Home) ##

## Developers:

TolyProg (Anatoly Mihailov) | main dev, kernel dev, project creator | Language: FASM assembler | donate: none