;   Copyright (C) 2020 Anatoly Mihailov (TolyProg)
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <https://www.gnu.org/licenses/>.

;______________________________________
;       Libs\Scan2ASCII                |
;______________________________________|
; "@" is a label                       |
; "?" is a function                    |
;______________________________________|

enter_key   =    0
bspace      =    0
tab         =    0
ctrl_key    =    0
lshift      =    0
rshift      =    0
prnscr      =    0
alt_key     =    0
caps        =    0
f1          =    0
f2          =    0
f3          =    0
f4          =    0
f5          =    0
f6          =    0
f7          =    0
f8          =    0
f9          =    0
f10         =    0
f11         =    0
f12         =    0
numlock     =    0
scroll      =    0
home        =    0
arrowup     =    0
pgup        =    0
num_sub     =    0
arrowleft   =    0
center5     =    0
arrowright  =    0
num_plus    =    0
_end        =    0
arrowdown   =    0
pgdn        =    0
_ins        =    0
del         =    0
?scan2ascii:
mov esi,eax
mov al,[esi+keymap]
ret
jmp ?panic
keymap:
    db    0
    db    '1234567890-=', bspace
    db    tab,'qwertyuiop[]',enter_key
    db    ctrl_key,'asdfghjkl;',39,'`',lshift
    db    '\','zxcvbnm,./',rshift,prnscr,alt_key,' '
    db    caps,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,numlock
    db    scroll,home,arrowup,pgup,num_sub,arrowleft,center5,arrowright
    db    num_plus,_end,arrowdown,pgdn,_ins,del